const roleHarvester = require('./role.harvester');
const roleUpgrader = require('./role.upgrader');
const roleBuilder = require('./role.builder');

const spawn1 = Game.spawns.Spawn1;

module.exports.loop = () => {
  Object.keys(Memory.creeps).forEach((key) => {
    if (!Game.creeps[key]) {
      delete Memory.creeps[key];
      console.log('Clearing non-existing creep memory:', key);
    }
  });

  const harvesters = _.filter(Game.creeps, creep => creep.memory.role === 'harvester');

  if (harvesters.length < 2) {
    const newName = spawn1.createCreep([WORK, WORK, CARRY, MOVE], undefined, {
      role: 'harvester',
    });
    console.log(`Spawning new harvester: ${newName}`);
  }

  Object.keys(Game.creeps).forEach((key) => {
    const creep = Game.creeps[key];
    if (creep.memory.role === 'harvester') {
      roleHarvester.run(creep);
    }
    if (creep.memory.role === 'upgrader') {
      roleUpgrader.run(creep);
    }
    if (creep.memory.role === 'builder') {
      roleBuilder.run(creep);
    }
  });
};
