module.exports = (grunt) => {
  grunt.loadNpmTasks('grunt-screeps');

  grunt.initConfig({
    screeps: {
      options: {
        branch: 'default',
        email: process.env.ScreepsEmail,
        password: process.env.ScreepsPassword,
        ptr: false,
      },
      dist: {
        src: ['src/*.js'],
      },
    },
  });
};
